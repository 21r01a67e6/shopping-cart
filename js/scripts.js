$(document).ready(function () {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];

    function updateCartCount() {
        let count = 0;
        cart.forEach(item => {
            count += item.quantity;
        });
        $('#cart-count').text(count);
    }

    function updateCartItems() {
        const $cartItems = $('#cart-items');
        $cartItems.empty();
        let total = 0;

        if (cart.length === 0) {
            $cartItems.append(`
            <h3 class="text-center my-4 text-muted">
                Your cart is empty, but that's okay! <br>
                We have some great products to choose from!
            </h3>
            `);
        } else {
            cart.forEach(item => {
                $cartItems.append(`
                <div class="cart-item">
                    <img src="${item.image}" class="img-fluid" alt="${item.title}">
                    <div>
                        <h5>${item.title}</h5>
                        <p>$${item.price.toFixed(2)}</p>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary btn-decrease" data-id="${item.id}">-</button>
                          </div>
                          <input type="text" class="form-control" value="${item.quantity}" readonly>
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary btn-increase" data-id="${item.id}">+</button>
                          </div>
                        </div>
                    </div>
                    <button class="btn-remove" data-id="${item.id}"><i class="fas fa-trash-alt"></i></button>
                </div>
                `);
                total += item.price * item.quantity;
            });
        }

        $('#total-price').text(total.toFixed(2));
    }

    function addToCart(product) {
        const existingProduct = cart.find(item => item.id === product.id);
        if (existingProduct) {
            existingProduct.quantity++;
        } else {
            product.quantity = 1;
            cart.push(product);
        }
        localStorage.setItem('cart', JSON.stringify(cart));
        updateCartCount();
    }

    function removeFromCart(productId) {
        const updatedCart = cart.filter(item => item.id !== productId);
        localStorage.setItem('cart', JSON.stringify(updatedCart));
        location.reload();
    }

    function changeQuantity(productId, delta) {
        const product = cart.find(item => item.id === productId);
        if (product) {
            product.quantity += delta;
            if (product.quantity <= 0) {
                removeFromCart(productId);
            } else {
                localStorage.setItem('cart', JSON.stringify(cart));
                updateCartCount();
                updateCartItems();
            }
        }
    }

    function fetchProducts() {
        $.ajax({
            url: 'https://fakestoreapi.com/products',
            method: 'GET',
            success: function (products) {
                products = products.sort(() => 0.5 - Math.random());
                renderProducts(products);
            }
        });
    }

    function renderProducts(products) {
        const $productList = $('#product-list');

        products.forEach(product => {
            $productList.append(`
          <div class="col-md-3">
            <div class="card product-card">
              <img src="${product.image}" class="card-img-top" alt="${product.title}">
              <div class="card-body">
                <h5 class="card-title">${product.title}</h5>
                <p class="card-text">$${product.price.toFixed(2)}</p>
                <button class="btn btn-primary add-to-cart" data-id="${product.id}">Add to Cart</button>
              </div>
            </div>
          </div>
        `);
        });
    }

    $(document).on('click', '.add-to-cart', function () {
        const id = $(this).data('id');
        $.ajax({
            url: `https://fakestoreapi.com/products/${id}`,
            method: 'GET',
            success: function (product) {
                addToCart(product);
            }
        });
    });

    $(document).on('click', '.btn-remove', function () {
        const id = $(this).data('id');
        removeFromCart(id);
    });

    $(document).on('click', '.btn-increase', function () {
        const id = $(this).data('id');
        changeQuantity(id, 1);
    });

    $(document).on('click', '.btn-decrease', function () {
        const id = $(this).data('id');
        changeQuantity(id, -1);
    });

    fetchProducts();
    updateCartCount();
    updateCartItems();
});
